# Timetable Image Taker

# Запуск проекта
```javascript
docker compose up -d --build
```


# Использование
Перейдите на адрес <a href="localhost:9001">localhost:9001</a> для просмотра minio 

# Запрос на примере google.com
Возврат PNG без кэширования
localhost:8800/schedule?url=https://google.com

  
  
Возврат PNG с сохранением в кэш  
localhost:8800/schedule?url=https://google.com&canCache=True