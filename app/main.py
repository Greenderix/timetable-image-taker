from quart import Quart, Response, request
from cache import get_cached_schedule
from schedule_gen import capture_screenshot


app = Quart(__name__)

@app.route('/schedule', methods=['GET'])
async def schedule():
    # Processing query parameters.
    url = request.args.get('url')
    can_cache = request.args.get('canCache', False)

    if url is None:
        return "Ссылка не указана"

    image_data = get_cached_schedule(url)
    if image_data:
        return Response(image_data, mimetype='image/png')
    else:
        screenshot = await capture_screenshot(url, can_cache)
        return Response(screenshot, mimetype='image/png')



if __name__ == '__main__':
    app.run(debug=True, port=8800, host='0.0.0.0')
