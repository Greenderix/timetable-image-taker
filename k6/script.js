import http from 'k6/http';
import { sleep } from 'k6';
export const options = {
  stages: [
    { duration: '15s', target: 1 }, // окрыть дашборд
    { duration: '30s', target: 1 }, // "прогрев" отрисовки на одном пользователе
    { duration: '30s', target: 3 }, // поднимаем количество пользователей, от 1 до 3 параметры не меняются
    { duration: '1m', target: 3 }, // минуту проверяем стабильность
    { duration: '1m', target: 10 }, // больше трех пользователей - увеличивается время на отрисовку
  ],
};
export default function () {
  http.get('http://localhost:8800/schedule?url=https://google.com');
  sleep(1);
}
